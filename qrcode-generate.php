<?php
/**
 * @package QR CODE Gnenerate
 * @version 1.0.1
 */
/*
Plugin Name: QR CODE Generate
Plugin URI: https://f0rmig4.gitlab.io/
Description: Esse plugin gera um link na pagina para gerar um qrcode
Author: Andre Formiga
Version: 1.0.1
Author URI: https://f0rmig4.gitlab.io/
*/


function show_qrcode_url() {
	$id = $_GET['post'];
	$url = get_permalink( $id );
	$post_type = get_post_type( $id );

	if ($post_type == 'posts'){
		echo "
		<style>
			#ctn-qr {
				line-height: 1.84615384;
				min-height: 25px;
				margin-top: 5px;
				padding: 0 10px;
				color: #666;
			}
		</style>
		<div id='ctn-qr'>
			<strong>Download do QR Code:</strong> 
			<a target='_blanck' href='/wp-content/plugins/qr-code-generate/generate.php?url=".$url."'>Baixe aqui</a>
		</div>";
	}
}

add_action( 'pre_wp_unique_post_slug', 'show_qrcode_url' );
