<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

if(isset($_GET['url'])){
    $url = 'https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl='.$_GET['url'].'&choe=UTF-8';

    $file_name = basename($url);
    header('Content-Type: application/octet-stream');
    header("Content-Transfer-Encoding: Binary");
    header("Content-disposition: attachment; filename=\"qrcode.png\"");
    readfile($url);
    exit;
}else{
    echo 'Não existe a imagem.';
}

?>