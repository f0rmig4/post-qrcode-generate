# post-qrcode-generate

Este plugin permite gerar QR Code de cada post do seu projeto dentro do painel do WordPress.

## instalação

- [ ] Envie a pasta `post-qrcode-generate` para o seu diretório `/wp-content/plugins/`.
- [ ] Ative o plugin na sessão de `Plugins` no painel administrativo do WordPress;
- [ ] Pronto, o link para gerar seu QR Code irá aparecer logo abaixo do `Link permanente` de cada artigo;

## autor
@f0rmig4